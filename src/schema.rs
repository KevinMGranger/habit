table! {
    habits (id) {
        id -> Integer,
        habit -> Text,
        event_type -> Bool,
        when -> Timestamp,
    }
}
