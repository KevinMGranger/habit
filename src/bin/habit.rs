#[macro_use]
extern crate structopt;
#[macro_use]
extern crate prettytable;
extern crate habit;
extern crate chrono;

use structopt::StructOpt;
use habit::{store, models};
use prettytable::Table;
use chrono::{NaiveDate,NaiveDateTime,NaiveTime};
use std::str::FromStr;

#[derive(StructOpt, Debug)]
struct List { }

impl List {
    fn call(self) {
        let connec = store::connect();
        let habits = store::all_habits(connec);

        let mut table = Table::new();
        table.add_row(row!["name", "good/bad", "when"]);

        for habit in habits {
            table.add_row(row![habit.habit, if habit.event_type {"+"} else {"-"}, habit.when]);
        }
        table.printstd();
    }
}

fn goodbad(input: &str) -> bool {
    match input {
        "+" => true,
        "-" => false,
        _ => panic!()
    }
}

#[derive(Debug)]
enum DateSpec {
    Both(NaiveDateTime),
    Date(NaiveDate),
    Time(NaiveTime)
}

impl FromStr for DateSpec {
    // TODO: custom error
    type Err = chrono::format::ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use DateSpec::*;
        if let Ok(ndt) = s.parse::<NaiveDateTime>() {
            Ok(Both(ndt))
        } else if let Ok(nd) = s.parse::<NaiveDate>() {
            Ok(Date(nd))
        } else {
            s.parse::<NaiveTime>().map(DateSpec::Time)
        }
    }
}

// TODO: verify correctness
impl From<DateSpec> for NaiveDateTime {
    fn from(ds: DateSpec) -> NaiveDateTime {
        use DateSpec::*;
        match ds {
            Both(x) => x,
            Date(x) => NaiveDateTime::new(x, chrono::Local::now().time()),
            Time(x) => NaiveDateTime::new(chrono::Local::today().naive_local(), x)
        }
    }
}

#[derive(StructOpt, Debug)]
struct Add {
    habit: String,
    #[structopt(parse(from_str="goodbad"))]
    event_type: bool,

    when: Option<DateSpec>
}

impl Add {
    fn call(self) {
        let connec = store::connect();
        let habit = models::NewHabitEvent { habit: &self.habit, event_type: self.event_type, when: self.when.map(From::from)};
        if let Ok(num) = store::add_habit(connec, habit) {
            println!("stored {} habits", num);
        } else {
            eprintln!("help");
        }
    }
}

#[derive(StructOpt, Debug)]
#[structopt(name = "cmd")]
enum Command {
    #[structopt(name = "list")] List(List),
    #[structopt(name = "add")] Add(Add)
}

impl Command {
    fn call(self) {
        match self {
            Command::List(x) => x.call(),
            Command::Add(x) => x.call()
        }
    }

}

fn main() {
    let cmd = Command::from_args();
    cmd.call();
}
