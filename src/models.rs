extern crate chrono;
use self::chrono::NaiveDateTime;
use super::schema::habits;

#[derive(Queryable, Debug)]
pub struct Habit {
    pub id: i32,
    pub habit: String,
    pub event_type: bool,
    pub when: NaiveDateTime,
}

#[derive(Insertable)]
#[table_name = "habits"]
pub struct NewHabitEvent<'a> {
    pub habit: &'a str,
    pub event_type: bool,
    pub when: Option<NaiveDateTime>,
}
