use super::models::*;

use diesel;
use diesel::prelude::*;
use diesel::sqlite::SqliteConnection;
use dotenv::dotenv;
use std::env;

pub fn connect() -> SqliteConnection {
    dotenv().ok();

    SqliteConnection::establish(
        env::var("DATABASE_URL")
            .expect("Need a DATABSE_URL")
            .as_ref(),
    ).expect("error connecting")
}

pub fn all_habits(connec: SqliteConnection) -> Vec<Habit> {
    use super::schema::habits::dsl::*;
    habits.load::<Habit>(&connec).unwrap()
}

pub fn add_habit<'a>(connec: SqliteConnection, hbt: NewHabitEvent<'a>) -> QueryResult<usize> {
    use super::schema::habits::dsl::*;
    diesel::insert_into(habits).values(&hbt).execute(&connec)
}